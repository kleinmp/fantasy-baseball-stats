<?php

namespace Drupal\fbase\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Filter form for player stat results.
 */
class PlayerFilter extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fbase_player_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filters = $this->getRequest()->query->get('filters');
    foreach (['year', 'name', 'positions', 'games_min', 'games_max'] as $key) {
      ${$key} = !empty($filters[$key]) ? $filters[$key] : NULL;
    }

    $years = range(2010, date('Y'));
    unset($years[array_search(2020, $years)]);
    $form['year'] = [
      '#type' => 'select',
      '#title' => $this->t('Year'),
      '#default_value' => $year,
      '#options' => [NULL => '-'] + array_combine($years, $years),
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $name,
    ];
    $form['positions'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => [
        NULL => '-',
        'C' => $this->t('C'),
        '1B' => $this->t('1B'),
        '2B' => $this->t('2B'),
        'SS' => $this->t('SS'),
        '3B' => $this->t('3B'),
        'OF' => $this->t('OF'),
        'UTIL' => $this->t('UTIL'),
        'SP' => $this->t('SP'),
        'RP' => $this->t('RP'),
      ],
      '#default_value' => $positions,
    ];
    $form['games_min'] = [
      '#type' => 'number',
      '#title' => $this->t('Games (min)'),
      '#default_value' => $games_min,
    ];
    $form['games_max'] = [
      '#type' => 'number',
      '#title' => $this->t('Games (max)'),
      '#default_value' => $games_max,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Queries'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query = [];
    $triggering_element = $form_state->getTriggeringElement();
    $values = $form_state->getValues();

    if ($triggering_element['#value'] != 'Clear Queries') {

      foreach (['sort', 'order'] as $key) {
        if ($this->getRequest()->query->get($key)) {
          $query[$key] = $this->getRequest()->query->get($key);
        }
      }
      foreach (['year', 'name', 'positions', 'games_min', 'games_max'] as $key) {
        if (!empty($values[$key])) {
          $query['filters'][$key] = $values[$key];
        }
      }

    }

    $route_name = $this->getRouteMatch()->getRouteName();
    $url = Url::fromRoute($route_name, $query);
    $form_state->setRedirectUrl($url);
  }

}
