<?php

namespace Drupal\fbase\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Filter form for daily stat results.
 */
class DailyFilter extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fbase_daily_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    foreach (['groups', 'filters'] as $key) {
      if ($this->getRequest()->query->get($key)) {
        ${$key} = $this->getRequest()->query->get($key);
      }
    }

    $form['groups'] = [
      '#type' => 'select',
      '#title' => $this->t('Groups'),
      '#multiple' => TRUE,
      '#options' => [
        'day_of_week' => $this->t('Day'),
        // 'day_of_month' => $this->t('Day of Month'),
        'month' => $this->t('Month'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => !empty($groups) ? $groups : NULL,
    ];
    $form['group_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Group Type'),
      '#options' => [
        'SUM' => $this->t('Totals'),
        'AVERAGE' => $this->t('Averages'),
      ],
      '#default_value' => $this->getRequest()->query->get('group_type') ?: 'SUM',
    ];

    $years = range(2010, date('Y'));
    unset($years[array_search(2020, $years)]);
    $form['year'] = [
      '#type' => 'select',
      '#title' => $this->t('Year'),
      '#default_value' => $filters['year'] ?? NULL,
      '#options' => [NULL => '-'] + array_combine($years, $years),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Queries'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query = [];
    $triggering_element = $form_state->getTriggeringElement();
    $values = $form_state->getValues();

    if ($triggering_element['#value'] != 'Clear Queries') {

      foreach ($this->getRequest()->query->all() as $key => $value) {
        if (!in_array($key, ['groups', 'group_type', 'filters'])) {
          $query[$key] = $value;
        }
      }
      if (!empty($values['groups'])) {
        $query['groups'] = $values['groups'];
        if (!empty($values['group_type'])) {
          $query['group_type'] = $values['group_type'];
        }
        if (empty($query['sorts'])) {
          $query['sorts'] = array_reverse($query['groups']);
          $query['sorts_dir'] = array_fill(0, count($query['groups']), 'ASC');
        }
      }
      if (!empty($values['year'])) {
        $query['filters']['year'] = $values['year'];
      }

    }

    $route_name = $this->getRouteMatch()->getRouteName();
    $url = Url::fromRoute($route_name, $query);
    $form_state->setRedirectUrl($url);
  }

}
