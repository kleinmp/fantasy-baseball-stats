<?php

namespace Drupal\fbase\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fbase\Plugin\StatLine\StatLineManager;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Compute the score on the given stats of a stat line.
 */
class ComputeScore extends FormBase {

  /**
   * Store the stat line manager service.
   *
   * @var Drupal\fbase\Plugin\StatLine\StatLineManager
   */
  protected $statLineManager;

  /**
   * Class constructor.
   *
   * @param Drupal\fbase\Plugin\StatLine\StatLineManager $stat_line_manager
   *   The state line manager service.
   */
  public function __construct(StatLineManager $stat_line_manager) {
    $this->statLineManager = $stat_line_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.fbase.stat_line_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fbase_compute_score';
  }

  /**
   * Get the stat line manager service.
   *
   * @return Drupal\fbase\Plugin\StatLine\StatLineManager
   *   The stat line manager.
   */
  public function getStatLineManager() : StatLineManager {
    return $this->statLineManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $stat_line_type = NULL) {
    $stat_line = $this->getStatLineManager()->createInstance($stat_line_type);
    $values = $this->getRequest()->query->get('stat_values');

    $form['stat_line'] = [
      '#type' => 'value',
      '#value' => $stat_line,
    ];
    $form['stats'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
    ];

    foreach ($stat_line->getStats() as $id => $stat) {
      if ($stat->computed() || $id == 'total') {
        continue;
      }
      $form['stats'][$id] = [
        '#type' => 'textfield',
        '#title' => $stat->getName(),
        '#default_value' => !empty($values[$id]) ? $values[$id] : NULL,
        '#size' => 5,
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $query = ['stat_values' => $values['stats']];

    $route_name = $this->getRouteMatch()->getRouteName();
    $url = Url::fromRoute($route_name, $query);
    $form_state->setRedirectUrl($url);
  }

}
