<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected hits stat.
 *
 * @Stat(
 *   id = "hits_projected",
 *   name = @Translation("Hits-Proj"),
 *   column = "hits",
 *   computed = TRUE,
 * )
 */
class HitsProjected extends Hits {}
