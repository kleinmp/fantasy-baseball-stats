<?php

namespace Drupal\fbase\Plugin\Stat;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for a stat.
 */
interface StatPluginInterface extends PluginInspectionInterface {

  /**
   * Return the name of the name of the service.
   *
   * @return string
   *   The short name of the stat.
   */
  public function getName() : string;

  /**
   * Get the related database column for this stat.
   *
   * @param string|null $group_type
   *   (optional) The grouping being done on the related query.
   *
   * @return string|null
   *   The related database column or NULL if none.
   */
  public function getColumn(?string $group_type = NULL) : ?string;

  /**
   * Get the number of decimals to display on this stat.
   *
   * @return int|null
   *   The number of decimals.
   */
  public function getDecimals() : ?int;

  /**
   * Whether a smaller number is best or larger (min or max).
   *
   * @return string|null
   *   The string 'min' or 'max'.
   */
  public function getBestOrder() : ?string;

  /**
   * Whether this stat should be included when totalling the stats.
   *
   * @return bool
   *   If this stat should be included in the total.
   */
  public function includeInTotal() : bool;

  /**
   * Whether this stat is computed from other data or not.
   *
   * @return bool
   *   If this is a computed stat or not.
   */
  public function computed() : bool;

  /**
   * Whether to display this stat or not.
   *
   * @return bool
   *   If the stat should be displayed.
   */
  public function display() : bool;

  /**
   * The sql alias for this expression.
   *
   * @return string
   *   The sql alias.
   */
  public function getExpressionAlias() : string;

  /**
   * Returns the value stored on this stat if it's a single piece of data.
   *
   * @return mixed
   *   The value of the stat.
   */
  public function getValue() : mixed;

  /**
   * Set the value on a single stat.
   *
   * @param mixed $value
   *   A new value for this stat.
   */
  public function setValue($value) : void;

  /**
   * Get the query expression for this stat.
   *
   * @param array $groups
   *   (optional) A list of groupings being used for the overall query.
   * @param string|null $group_type
   *   (optional) The type of grouping being run (SUM, AVERAGE).
   *
   * @return string|null
   *   The query expression.
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : ?string;

  /**
   * Compute the value on this single stat.
   *
   * @return mixed
   *   The computed value.
   */
  public function compute() : mixed;

  /**
   * Get a required stat on this stat.
   *
   * @param string $id
   *   The id of the stat to return.
   *
   * @return \Drupal\fbase\Plugin\Stat\StatPluginInterface
   *   The stat object itself.
   */
  public function getStat(string $id) : StatPluginInterface;

}
