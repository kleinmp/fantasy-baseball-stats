<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides month stat.
 *
 * @Stat(
 *   id = "month",
 *   name = @Translation("Month"),
 *   best = NULL,
 *   column = "month",
 * )
 */
class Month extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $expression = $this->getColumn();
    if (!empty($groups)) {
      if (!in_array('month', $groups)) {
        $expression = '';
      }
    }
    return $expression;
  }

}
