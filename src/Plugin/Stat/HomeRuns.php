<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides home runs stat.
 *
 * @Stat(
 *   id = "home_runs",
 *   name = @Translation("HRs"),
 *   column = "home_runs",
 * )
 */
class HomeRuns extends StatPluginBase {}
