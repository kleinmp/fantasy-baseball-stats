<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected home runs stat.
 *
 * @Stat(
 *   id = "home_runs_projected",
 *   name = @Translation("HRs-Proj"),
 *   column = "home_runs",
 *   computed = TRUE,
 * )
 */
class HomeRunsProjected extends ProjectedStat {}
