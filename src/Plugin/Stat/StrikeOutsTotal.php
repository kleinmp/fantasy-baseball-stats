<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides runs total stat.
 *
 * @Stat(
 *   id = "strikeouts_total",
 *   name = @Translation("SOs-Tot"),
 *   computed = TRUE,
 *   constant = 1.35,
 *   required_stat_ids = {
 *     "strikeouts",
 *   },
 *   total = TRUE,
 * )
 */
class StrikeOutsTotal extends StatTotal {}
