<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides innings pitched stat.
 *
 * @Stat(
 *   id = "innings_pitched",
 *   name = @Translation("IPs"),
 *   column = "innings_pitched",
 *   decimals = 1,
 * )
 */
class InningsPitched extends StatPluginBase {}
