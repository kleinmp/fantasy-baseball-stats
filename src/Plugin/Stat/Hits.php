<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides hits stat.
 *
 * @Stat(
 *   id = "hits",
 *   name = @Translation("Hits"),
 *   column = "hits",
 * )
 */
class Hits extends StatPluginBase {}
