<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides positions stat.
 *
 * @Stat(
 *   id = "positions",
 *   name = @Translation("Positions"),
 *   best = NULL,
 *   column = "positions",
 *   decimals = NULL,
 * )
 */
class Positions extends StatPluginBase {}
