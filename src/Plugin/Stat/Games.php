<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides games stat.
 *
 * @Stat(
 *   id = "games",
 *   name = @Translation("Games"),
 *   column = "games",
 * )
 */
class Games extends StatPluginBase {

  /**
   * The total number of game played by individual.
   *
   * @var int
   */
  protected int $totalGames = 162;

  /**
   * Get the total number of possible games.
   *
   * @return int
   *   The total number of games.
   */
  public function getTotalGames() : int {
    return $this->totalGames;
  }

}
