<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides batting average stat.
 *
 * @Stat(
 *   id = "batting_average",
 *   name = @Translation("BA"),
 *   computed = TRUE,
 *   decimals = 3,
 *   required_stat_ids = {
 *     "at_bats",
 *     "hits",
 *   }
 * )
 */
class BattingAverage extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('hits')->getValue() / $this->getStat('at_bats')->getValue();
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }
    return $this->getStat('hits')->getColumn($group_type) . ' / ' . $this->getStat('at_bats')->getColumn($group_type);
  }

}
