<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides saves total stat.
 *
 * @Stat(
 *   id = "saves_total",
 *   name = @Translation("Saves-Tot"),
 *   constant = 17,
 *   computed = TRUE,
 *   required_stat_ids = {
 *     "saves",
 *   }
 * )
 */
class SavesTotal extends StatTotal {}
