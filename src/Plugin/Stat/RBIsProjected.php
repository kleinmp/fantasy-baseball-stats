<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected RBIs stat.
 *
 * @Stat(
 *   id = "rbis_projected",
 *   name = @Translation("RBIs-Proj"),
 *   column = "rbis",
 *   computed = TRUE,
 * )
 */
class RBIsProjected extends ProjectedStat {}
