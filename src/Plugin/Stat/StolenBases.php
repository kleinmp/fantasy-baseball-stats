<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides at bats stat.
 *
 * @Stat(
 *   id = "stolen_bases",
 *   name = @Translation("SBs"),
 *   column = "stolen_bases",
 * )
 */
class StolenBases extends StatPluginBase {}
