<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides batting average total stat.
 *
 * @Stat(
 *   id = "batting_average_total",
 *   name = @Translation("BA-Tot"),
 *   computed = TRUE,
 *   constant = 4.65,
 *   required_stat_ids = {
 *     "batting_average",
 *     "hits",
 *   }
 * )
 */
class BattingAverageTotal extends StatTotal {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $group_type = !empty($groups) ? $group_type : NULL;
    return $this->getStat('hits')->getColumn($group_type) . ' * (' . $this->getStat('batting_average')->getExpression($groups, $group_type) . ') * ' . $this->getConstant();
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('hits')->getValue() * $this->getStat('batting_average')->getValue() * $this->getConstant();
    $this->setValue($value);
    return $value;
  }

}
