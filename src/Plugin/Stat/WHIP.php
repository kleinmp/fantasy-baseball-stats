<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides whip stat.
 *
 * @Stat(
 *   id = "whip",
 *   name = @Translation("WHIP"),
 *   best = "min",
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "base_runners",
 *     "innings_pitched",
 *   },
 * )
 */
class WHIP extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('base_runners')->getValue() / $this->getStat('innings_pitched')->getValue();
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') :string {
    if (empty($groups)) {
      $group_type = NULL;
    }
    return $this->getStat('base_runners')->getColumn($group_type) . ' / ' . $this->getStat('innings_pitched')->getColumn($group_type);
  }

}
