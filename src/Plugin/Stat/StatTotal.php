<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Definition of a basic total stat for valuing a stat.
 */
abstract class StatTotal extends StatPluginBase {

  /**
   * Constructs a Drupal\fbase\Plugin\Stat\ProjectedStat object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $plugin_definition['total'] = TRUE;
    $plugin_definition['computed'] = TRUE;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM')  :string {
    $group_type = !empty($groups) ? $group_type : NULL;
    return $this->getBaseStat()->getColumn($group_type) . ' * ' . $this->getConstant();
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getBaseStat()->getValue() * $this->getConstant();
    $this->setValue($value);
    return $value;
  }

  /**
   * Get the plugin id of the base stat.
   *
   *   A basic total is based on one required stat.
   *
   * @return string
   *   The id of the base stat.
   */
  public function getBaseStatId() : string {
    return reset($this->requiredStatIds);
  }

  /**
   * Get the constant for computing the value.
   *
   * @return int|float
   *   The value of the constat.
   */
  public function getConstant() : int|float {
    return $this->constant;
  }

  /**
   * Get the related base stat instance.
   *
   * @return \Drupal\fbase\Plugin\Stat\StatPluginInterface
   *   The base stat object.
   */
  public function getBaseStat() : StatPluginInterface {
    return $this->getStat($this->getBaseStatId());
  }

}
