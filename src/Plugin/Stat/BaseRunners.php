<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides base runners stat.
 *
 * @Stat(
 *   id = "base_runners",
 *   name = @Translation("BRs"),
 *   best = "min",
 *   column = "base_runners",
 * )
 */
class BaseRunners extends StatPluginBase {}
