<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides total stat which sums all totals.
 *
 * @Stat(
 *   id = "total",
 *   name = @Translation("Total"),
 *   computed = TRUE,
 * )
 */
class Total extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {

    // Require all stats that are totals.
    if (!empty($configuration['stats'])) {
      foreach ($configuration['stats'] as $id => $stat) {
        if ($stat->includeInTotal()) {
          $plugin_definition['required_stat_ids'][] = $id;
        }
      }
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $total_expression = '';
    foreach ($this->requiredStats as $stat) {
      $prefix = !empty($total_expression) ? ' + ' : '';
      $total_expression .= $prefix . $stat->getExpression($groups, $group_type);
    }
    return $total_expression;
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $total = 0;
    foreach ($this->requiredStats as $stat) {
      $total += $stat->getValue();
    }
    $this->setValue($total);
    return $total;
  }

}
