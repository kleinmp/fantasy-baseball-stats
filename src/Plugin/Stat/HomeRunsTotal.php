<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides home runs total stat.
 *
 * @Stat(
 *   id = "home_runs_total",
 *   name = @Translation("HRs-Tot"),
 *   computed = TRUE,
 *   constant = 7,
 *   required_stat_ids = {
 *     "home_runs",
 *   },
 *   total = TRUE,
 * )
 */
class HomeRunsTotal extends StatTotal {}
