<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides stolen bases total stat.
 *
 * @Stat(
 *   id = "stolen_bases_total",
 *   name = @Translation("SBs-Tot"),
 *   computed = TRUE,
 *   constant = 9,
 *   required_stat_ids = {
 *     "stolen_bases",
 *   },
 *   total = TRUE,
 * )
 */
class StolenBasesTotal extends StatTotal {}
