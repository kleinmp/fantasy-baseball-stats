<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides total per estimated games stat (hitting).
 *
 * @Stat(
 *   id = "total_per_est_games",
 *   name = @Translation("Tot-per-game"),
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "days",
 *     "total",
 *   },
 * )
 */
class TotalPerEstimatedGames extends TotalPerDays {

  /**
   * Number of estimated games played per day.
   *
   *   162 games * 10 positions / 180 total days.
   *
   * @var int
   */
  protected $gamesPerDay = 9;

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      return '';
    }
    return '(' . parent::getExpression($groups, $group_type) . ') / (' . $this->gamesPerDay . ' * 30)';
  }

}
