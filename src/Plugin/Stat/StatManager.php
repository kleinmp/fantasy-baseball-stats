<?php

namespace Drupal\fbase\Plugin\Stat;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manage Stats.
 */
class StatManager extends DefaultPluginManager {

  /**
   * Constructs an StatManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Stat',
      $namespaces,
      $module_handler,
      'Drupal\fbase\Plugin\Stat\StatPluginInterface',
      'Drupal\fbase\Annotation\Stat',
    );

    $this->alterInfo('fbase_stat_info');
    $this->setCacheBackend($cache_backend, 'fbase.stat');
  }

}
