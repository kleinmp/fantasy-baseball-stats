<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides teams stat.
 *
 * @Stat(
 *   id = "teams",
 *   name = @Translation("Teams"),
 *   best = NULL,
 *   column = "teams",
 *   decimals = NULL,
 * )
 */
class Teams extends StatPluginBase {}
