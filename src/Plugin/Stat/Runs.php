<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides runs stat.
 *
 * @Stat(
 *   id = "runs",
 *   name = @Translation("Runs"),
 *   column = "runs"
 * )
 */
class Runs extends StatPluginBase {}
