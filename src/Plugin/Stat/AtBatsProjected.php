<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected at bats stat.
 *
 * @Stat(
 *   id = "at_bats_projected",
 *   name = @Translation("ABs-Proj"),
 *   column = "at_bats",
 *   computed = TRUE,
 * )
 */
class AtBatsProjected extends ProjectedStat {}
