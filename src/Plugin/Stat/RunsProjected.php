<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected runs stat.
 *
 * @Stat(
 *   id = "runs_projected",
 *   name = @Translation("Runs-Proj"),
 *   column = "runs",
 *   computed = TRUE,
 * )
 */
class RunsProjected extends StatPluginBase {}
