<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected games stat.
 *
 * @Stat(
 *   id = "games_projected",
 *   name = @Translation("Games-Proj"),
 *   column = "games",
 * )
 */
class GamesProjected extends StatPluginBase {

  /**
   * The total number of games played.
   *
   * @var int
   */
  protected $totalGames = 162;

  /**
   * {@inheritdoc}
   */
  public function getValue() : mixed {
    return $this->getTotalGames();
  }

  /**
   * Get the total number of games played.
   *
   * @return int
   *   The total number of games played.
   */
  public function getTotalGames() : int {
    return $this->totalGames;
  }

}
