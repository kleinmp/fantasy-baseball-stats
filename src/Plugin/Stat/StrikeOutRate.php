<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides strike out rate stat.
 *
 * @Stat(
 *   id = "strikeout_rate",
 *   name = @Translation("SO Rate"),
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "strikeouts",
 *     "innings_pitched",
 *   },
 * )
 */
class StrikeOutRate extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('strikeouts')->getValue() / $this->getStat('innings_pitched')->getValue();
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }
    return $this->getStat('strikeouts')->getColumn($group_type) . ' / ' . $this->getStat('innings_pitched')->getColumn($group_type);
  }

}
