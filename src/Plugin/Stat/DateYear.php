<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides year stat.
 *
 * @Stat(
 *   id = "date_year",
 *   name = @Translation("Year"),
 *   column = "date",
 *   expression_alias = "year",
 * )
 */
class DateYear extends Date {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $expression = 'DATE_FORMAT(' . $this->getColumn() . ', \'%Y\')';
    if (!empty($groups)) {
      if (!in_array('year', $groups)) {
        $expression = '';
      }
    }
    return $expression;
  }

}
