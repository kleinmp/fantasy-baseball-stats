<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides at bats stat.
 *
 * @Stat(
 *   id = "at_bats",
 *   name = @Translation("ABs"),
 *   column = "at_bats",
 * )
 */
class AtBats extends StatPluginBase {}
