<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides total per days stat.
 *
 * @Stat(
 *   id = "total_per_days",
 *   name = @Translation("Tot-per"),
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "days",
 *     "total",
 *   },
 * )
 */
class TotalPerDays extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      return '';
    }
    $total_expression = $this->getStat('total')->getExpression($groups, $group_type);
    $days_expression = $this->getStat('days')->getExpression($groups, $group_type);
    return '(' . $total_expression . ') / ' . $days_expression . ' * 30';
  }

}
