<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides earned runs stat.
 *
 * @Stat(
 *   id = "earned_runs",
 *   name = @Translation("ERs"),
 *   beat = "min",
 *   column = "earned_runs",
 * )
 */
class EarnedRuns extends StatPluginBase {}
