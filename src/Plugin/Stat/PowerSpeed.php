<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides home runs plus stolen bases stat.
 *
 * @Stat(
 *   id = "power_speed",
 *   name = @Translation("HR+SB"),
 *   computed = TRUE,
 *   required_stat_ids = {
 *     "home_runs",
 *     "stolen_bases",
 *   },
 * )
 */
class PowerSpeed extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('home_runs')->getValue() + $this->getStat('stolen_bases')->getValue();
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }
    return $this->getStat('home_runs')->getColumn($group_type) . ' + ' . $this->getStat('stolen_bases')->getColumn($group_type);
  }

}
