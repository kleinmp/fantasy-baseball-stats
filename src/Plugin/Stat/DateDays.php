<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides days stat.
 *
 * @Stat(
 *   id = "days",
 *   name = @Translation("Days"),
 *   column = "date",
 * )
 */
class DateDays extends Date {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (!empty($groups)) {
      return 'COUNT(' . $this->getColumn() . ')';
    }
    return '';
  }

}
