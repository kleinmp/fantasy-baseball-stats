<?php

namespace Drupal\fbase\Plugin\Stat;

use Drupal\fbase\Plugin\Stat\StatPluginBase;

/**
 * Provides wins stat.
 *
 * @Stat(
 *   id = "wins",
 *   name = @Translation("Wins"),
 *   column = "wins",
 * )
 */
class Wins extends StatPluginBase {}
