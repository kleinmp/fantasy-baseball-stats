<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides whip total stat.
 *
 * @Stat(
 *   id = "whip_total",
 *   name = @Translation("WHIP-Tot"),
 *   computed = TRUE,
 *   constant = 2100,
 *   required_stat_ids = {
 *     "base_runners",
 *     "innings_pitched",
 *   },
 *   total = TRUE,
 * )
 */
class WHIPTotal extends StatPluginBase {

  /**
   * Total number of innings possible.
   */
  const TOTAL_INNINGS = 1400;

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $ip_value = $this->getStat('innings_pitched')->getValue();
    $br_value = $this->getStat('base_runners')->getValue();
    $value = $ip_value * $ip_value / ($br_value * static::TOTAL_INNINGS) * $this->constant;
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }

    $ip_column = $this->getStat('innings_pitched')->getColumn($group_type);
    $br_column = $this->getStat('base_runners')->getColumn($group_type);
    return $ip_column . ' * ' . $ip_column . ' / (' . $br_column . ' * ' . static::TOTAL_INNINGS . ') * ' . $this->constant;
  }

}
