<?php

namespace Drupal\fbase\Plugin\Stat;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an the base plugin for a stat.
 */
abstract class StatPluginBase extends PluginBase implements StatPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Whether a higher or lower number is better (min or max).
   *
   * @var string|null
   */
  protected ?string $best = 'max';

  /**
   * The name of the related sql table column.
   *
   * @var string|null
   */
  protected ?string $column = NULL;

  /**
   * The value used to compute the total value.
   *
   * @var int|float|null
   */
  protected int|float|null $constant = NULL;

  /**
   * Whether or not this is a computed value.
   *
   * @var bool
   */
  protected bool $computed = FALSE;

  /**
   * The number of decimals to display on this number.
   *
   * @var int|null
   */
  protected ?int $decimals = 0;

  /**
   * Whether or not to display the stat.
   *
   * @var bool
   */
  protected bool $display = TRUE;

  /**
   * The sql alias to use.
   *
   * @var string
   */
  protected string $expressionAlias = '';

  /**
   * The required stat objects.
   *
   * @var array
   */
  protected array $requiredStats = [];

  /**
   * The plugin ids of the required stats.
   *
   * @var array
   */
  protected array $requiredStatIds = [];

  /**
   * Whether or not this is used to compute the total score.
   *
   * @var bool
   */
  protected bool $total = FALSE;

  /**
   * The value when used as a single stat.
   *
   * @var mixed
   */
  protected $value;

  /**
   * Constructs a Drupal\fbase\Plugin\Stat\StatPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    foreach ([
      'best',
      'column',
      'constant',
      'computed',
      'decimals',
      'display',
      'total',
    ] as $property) {
      if (array_key_exists($property, $plugin_definition)) {
        $this->{$property} = $plugin_definition[$property];
      }
    }
    if (!empty($plugin_definition['expression_alias'])) {
      $this->expressionAlias += $plugin_definition['expression_alias'];
    }
    if (!empty($plugin_definition['required_stat_ids'])) {
      $this->requiredStatIds += $plugin_definition['required_stat_ids'];
    }

    if (!empty($this->requiredStatIds)) {
      foreach ($this->requiredStatIds as $id) {
        if (empty($configuration['stats'][$id])) {
          throw new PluginException(sprintf('Missing required stat %s', $id));
        }
        $this->requiredStats[$id] = $configuration['stats'][$id];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() : string {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getColumn(?string $group_type = NULL) : ?string {
    return $group_type ? $group_type . '(' . $this->column . ')' : $this->column;
  }

  /**
   * {@inheritdoc}
   */
  public function getDecimals() : ?int {
    return $this->decimals;
  }

  /**
   * {@inheritdoc}
   */
  public function getBestOrder() : ?string {
    return $this->best;
  }

  /**
   * {@inheritdoc}
   */
  public function includeInTotal() : bool {
    return $this->total;
  }

  /**
   * {@inheritdoc}
   */
  public function computed() : bool {
    return $this->computed;
  }

  /**
   * {@inheritdoc}
   */
  public function display() : bool {
    return $this->display;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpressionAlias() : string {
    return $this->expressionAlias;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() : mixed {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue(mixed $value) : void {
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : ?string {
    return $this->getColumn(!empty($groups) ? $group_type : NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    return $this->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getStat(string $id) : StatPluginInterface {
    // @todo throw exception if stat not found, remove null return.
    return $this->requiredStats[$id];
  }

}
