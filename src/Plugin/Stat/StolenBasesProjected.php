<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides projected stolen bases stat.
 *
 * @Stat(
 *   id = "stolen_bases_projected",
 *   name = @Translation("SBs-Proj"),
 *   column = "stolen_bases",
 *   computed = TRUE,
 * )
 */
class StolenBasesProjected extends ProjectedStat {}
