<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides saves stat.
 *
 * @Stat(
 *   id = "saves",
 *   name = @Translation("Saves"),
 *   column = "saves"
 * )
 */
class Saves extends StatPluginBase {}
