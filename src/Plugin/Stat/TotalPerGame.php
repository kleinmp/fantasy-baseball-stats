<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides total per game stat.
 *
 * @Stat(
 *   id = "total_per_game",
 *   name = @Translation("Tot-per"),
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "games",
 *     "total",
 *   },
 * )
 */
class TotalPerGame extends StatPluginBase {

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $total = $this->getStat('total')->getValue();
    $games = $this->getStat('games')->getValue();
    return $total / $games;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $total_expression = $this->getStat('total')->getExpression($groups, $group_type);
    $game_expression = $this->getStat('games')->getColumn();
    return '(' . $total_expression . ') / ' . $game_expression;
  }

}
