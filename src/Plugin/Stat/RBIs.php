<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides RBIs stat.
 *
 * @Stat(
 *   id = "rbis",
 *   name = @Translation("RBIs"),
 *   column = "rbis",
 * )
 */
class RBIs extends StatPluginBase {}
