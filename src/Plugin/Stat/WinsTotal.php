<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides wins total stat.
 *
 * @Stat(
 *   id = "wins_total",
 *   name = @Translation("Wins-Tot"),
 *   computed = TRUE,
 *   constant = 17,
 *   required_stat_ids = {
 *     "wins",
 *   },
 *   total = TRUE,
 * )
 */
class WinsTotal extends StatTotal {}
