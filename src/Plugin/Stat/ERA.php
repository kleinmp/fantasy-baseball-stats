<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides era stat.
 *
 * @Stat(
 *   id = "era",
 *   name = @Translation("ERA"),
 *   best = "min",
 *   computed = TRUE,
 *   decimals = 2,
 *   required_stat_ids = {
 *     "innings_pitched",
 *     "earned_runs",
 *   },
 * )
 */
class ERA extends StatPluginBase {

  /**
   * Inning per game.
   */
  const INNINGS_PER_GAME = 9;

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $value = $this->getStat('earned_runs')->getValue() / $this->getStat('innings_pitched')->getValue() * static::INNINGS_PER_GAME;
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }
    return $this->getStat('earned_runs')->getColumn($group_type) . ' / ' . $this->getStat('innings_pitched')->getColumn($group_type) . ' * ' . static::INNINGS_PER_GAME;
  }

}
