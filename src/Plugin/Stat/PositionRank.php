<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides position rank stat.
 *
 * @Stat(
 *   id = "position_rank",
 *   name = @Translation("Pos. Rank"),
 *   best = NULL,
 *   column = "position_rank",
 *   decimals = NULL,
 * )
 */
class PositionRank extends StatPluginBase {}
