<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides era total stat.
 *
 * @Stat(
 *   id = "era_total",
 *   name = @Translation("ERA-Tot"),
 *   computed = TRUE,
 *   constant = 650,
 *   required_stat_ids = {
 *     "earned_runs",
 *     "innings_pitched",
 *   },
 *   total = TRUE,
 * )
 */
class ERATotal extends StatPluginBase {
  /**
   * The total possible number of innings.
   */
  const TOTAL_INNINGS = 1400;

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $ip_value = $this->getStat('innings_pitched')->getValue();
    $er_value = $this->getStat('earned_runs')->getValue();
    $value = $ip_value * $ip_value / ($er_value * static::TOTAL_INNINGS) * $this->constant;
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (empty($groups)) {
      $group_type = NULL;
    }

    $ip_column = $this->getStat('innings_pitched')->getColumn($group_type);
    $er_column = $this->getStat('earned_runs')->getColumn($group_type);
    return $ip_column . ' * ' . $ip_column . ' / (' . $er_column . ' * ' . static::TOTAL_INNINGS . ') * ' . $this->constant;
  }

}
