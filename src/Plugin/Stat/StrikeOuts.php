<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides strikeouts stat.
 *
 * @Stat(
 *   id = "strikeouts",
 *   name = @Translation("Strike Outs"),
 *   column = "strikeouts",
 * )
 */
class StrikeOuts extends StatPluginBase {}
