<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides class for projected stats.
 */
abstract class ProjectedStat extends StatPluginBase {

  /**
   * Constructs a Drupal\fbase\Plugin\Stat\ProjectedStat object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $plugin_definition['required_stat_ids'][] = 'games';
    $plugin_definition['computed'] = TRUE;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : mixed {
    $games = $this->getStat('games');
    $value = $games->getTotalGames() / $games->getValue() * $this->getValue();
    $this->setValue($value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    $group_type = !empty($groups) ? $group_type : NULL;
    $games = $this->getStat('games');
    return $games->getTotalGames() . " / " . $games->getColumn($group_type) . " * " . $this->getColumn($group_type);
  }

}
