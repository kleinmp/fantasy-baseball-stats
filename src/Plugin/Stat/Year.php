<?php

namespace Drupal\fbase\Plugin\Stat;

use Drupal\fbase\Plugin\Stat\StatPluginBase;

/**
 * Provides year stat.
 *
 * @Stat(
 *   id = "year",
 *   name = @Translation("Year"),
 *   best = NULL,
 *   column = "year",
 * )
 */
class Year extends StatPluginBase {}
