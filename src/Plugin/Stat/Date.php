<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides date stat.
 *
 * @Stat(
 *   id = "date",
 *   name = @Translation("Date"),
 *   best = NULL,
 *   column = "date",
 *   decimals = NULL,
 *   display = FALSE,
 * )
 */
class Date extends StatPluginBase {}
