<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides rank stat.
 *
 *   Rank became a reserved word in Mysql 8
 *   so we add quotes.
 *
 * @Stat(
 *   id = "rank",
 *   name = @Translation("Rank"),
 *   best = NULL,
 *   column = "`rank`",
 *   decimals = NULL,
 * )
 */
class Rank extends StatPluginBase {}
