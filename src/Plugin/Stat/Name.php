<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides name stat.
 *
 * @Stat(
 *   id = "name",
 *   name = @Translation("Name"),
 *   best = NULL,
 *   column = "name",
 *   decimals = NULL,
 * )
 */
class Name extends StatPluginBase {}
