<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides runs total stat.
 *
 * @Stat(
 *   id = "runs_total",
 *   name = @Translation("Runs-Tot"),
 *   computed = TRUE,
 *   constant = 2.25,
 *   required_stat_ids = {
 *     "runs",
 *   },
 *   total = TRUE,
 * )
 */
class RunsTotal extends StatTotal {}
