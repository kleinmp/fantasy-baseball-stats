<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides rbis total stat.
 *
 * @Stat(
 *   id = "rbis_total",
 *   name = @Translation("RBIs-Tot"),
 *   computed = TRUE,
 *   constant = 2.25,
 *   required_stat_ids = {
 *     "rbis",
 *   },
 *   total = TRUE,
 * )
 */
class RBIsTotal extends StatTotal {}
