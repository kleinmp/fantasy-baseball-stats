<?php

namespace Drupal\fbase\Plugin\Stat;

/**
 * Provides day of week stat.
 *
 * @Stat(
 *   id = "day_of_week",
 *   name = @Translation("DOW"),
 *   column = "date",
 *   decimals = NULL,
 * )
 */
class DateDOW extends Date {

  /**
   * {@inheritdoc}
   */
  public function getExpression(array $groups = [], ?string $group_type = 'SUM') : string {
    if (!empty($groups)) {
      return '';
    }
    return 'DATE_FORMAT(' . $this->getColumn() . ', \'%W\')';
  }

}
