<?php

namespace Drupal\fbase\Plugin\StatLine;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\fbase\Plugin\Stat\StatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an interface for a stat line.
 */
abstract class StatLinePluginBase extends PluginBase implements StatLinePluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Stat Manager service.
   *
   * @var \Drupal\fbase\Plugin\Stat\StatManager
   */
  protected StatManager $statManager;

  /**
   * The sql database name related to this stat line.
   *
   * @var string
   */
  protected string $table;

  /**
   * List of related stat ids in display order.
   *
   * @var array
   */
  protected array $statOrder;

  /**
   * List of stat instances related to this stat line.
   *
   * @var array
   *   Array of \Drupal\fbase\Plugin\Stat\StatPluginInterface
   */
  protected array $stats = [];

  /**
   * Metadata about each stat.
   *
   * @var array
   */
  protected array $statMetaData = [];

  /**
   * The name of the stat to sort by default.
   *
   * @var string
   */
  protected string $defaultSort;

  /**
   * Table header data for displaying stats.
   *
   * @var array
   */
  protected array $header = [];

  /**
   * The query results are stored here.
   *
   * @var array
   */
  protected $results;

  /**
   * Allowed modifiers for the sql expression.
   *
   * @var array
   */
  protected array $modifiers = [
    'sorts' => [],
    'filters' => [],
    'groups' => [],
    'group_type' => 'SUM',
  ];

  /**
   * Constructs a Drupal\fbase\Plugin\StatLine\StatLinePluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The current database connection.
   * @param \Drupal\fbase\Plugin\Stat\StatManager $stat_manager
   *   The stat manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, StatManager $stat_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->database = $database;
    $this->statManager = $stat_manager;

    if (!empty($plugin_definition['table'])) {
      $this->table = $plugin_definition['table'];
    }
    if (!empty($plugin_definition['stat_order'])) {
      $this->statOrder = $plugin_definition['stat_order'];
    }
    if (!empty($plugin_definition['default_sort'])) {
      $this->defaultSort = $plugin_definition['default_sort'];
    }

    foreach ($this->statOrder as $id) {
      // \Drupal\Component\Plugin\Exception\PluginException thrown
      // if id does not exist or if required stats not there.
      $this->stats[$id] = $this->getStatManager()->createInstance($id, ['stats' => $this->stats]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('plugin.manager.fbase.stat_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() : string {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabase() : Connection {
    return $this->database;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatManager() : StatManager {
    return $this->statManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getStats() : array {
    return $this->stats;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader() : array {
    if (!empty($this->header)) {
      return $this->header;
    }

    foreach ($this->getStatMetaData() as $field => $data) {
      $alias = !empty($data['alias']) ? $data['alias'] : $field;
      if (!empty($data['display'])) {
        $this->header[] = [
          'data' => $data['title'],
          'field' => $alias,
          'initial_click_sort' => $data['best'] == 'max' ? 'desc' : 'asc',
          'sort' => $field === $this->defaultSort ? 'desc' : NULL,
        ];
      }
    }
    return $this->header;
  }

  /**
   * {@inheritdoc}
   */
  public function results() : array {
    return $this->results;
  }

  /**
   * {@inheritdoc}
   */
  public function statsInDb() : bool {
    return isset($this->table);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatTitles() : array {
    $statMetaData = $this->getStatMetaData();
    return array_column($statMetaData, 'title');
  }

  /**
   * {@inheritdoc}
   */
  public function getStatMetaData() : array {
    if (!empty($this->statMetaData)) {
      return $this->statMetaData;
    }

    foreach ($this->getStats() as $id => $stat) {
      if ($expression = $stat->getExpression($this->modifiers['groups'], $this->modifiers['group_type'])) {
        $this->statMetaData[$id] = [
          'expression' => $expression,
          'display' => $stat->display(),
          'title' => $stat->getName(),
          'best' => $stat->getBestOrder(),
          'decimals' => $stat->getDecimals(),
          'alias' => $stat->getExpressionAlias(),
        ];
      }
    }

    return $this->statMetaData;
  }

  /**
   * {@inheritdoc}
   */
  public function executeQuery() : StatLinePluginInterface {

    $query = $this->getDatabase()->select($this->table, 'sl');

    foreach ($this->getStatMetaData() as $field => $data) {
      $alias = !empty($data['alias']) ? $data['alias'] : $field;
      $query->addExpression($data['expression'], $alias);
    }

    if (!empty($this->modifiers['groups'])) {
      foreach ($this->modifiers['groups'] as $group) {
        $query->groupBy($group);
      }
    }

    if (!empty($this->modifiers['filters'])) {
      foreach ($this->modifiers['filters'] as $filter) {
        $query->condition($filter['column'], $filter['value'], $filter['operator'] ?: '=');
      }
    }

    if (!empty($this->modifiers['sorts'])) {
      foreach ($this->modifiers['sorts'] as $sort) {
        $query->orderBy($sort['column'], $sort['direction'] ?: 'ASC');
      }
    }

    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($this->getHeader());
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(200);

    $this->results = $pager->execute()->fetchAll();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addModifier($type, $modifier) : StatLinePluginInterface {
    if (!isset($this->modifiers[$type])) {
      throw new \Exception(sprintf('Invalid modifier %s', $type));
    }

    $this->modifiers[$type][] = $modifier;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setModifier(string $type, array $modifiers) : StatLinePluginInterface {
    if (!isset($this->modifiers[$type])) {
      throw new \Exception(sprintf('Invalid modifier %s', $type));
    }

    foreach ($modifiers as $modifier) {
      $this->addModifier($type, $modifier);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface {
    foreach ($this->modifiers as $type => $current) {
      if (isset($modifiers[$type])) {
        if (!is_array($modifiers[$type])) {
          $this->addModifier($type, $modifiers[$type]);
        }
        else {
          $this->setModifier($type, $modifiers[$type]);
        }
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatValues(array $stat_values) : StatLinePluginInterface {
    $stats = $this->getStats();
    foreach ($stat_values as $id => $value) {
      if (empty($stats[$id])) {
        throw \Exception(sprintf('Stat %s does not exist on stat line.', $id));
      }
      $stats[$id]->setValue($value);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function compute() : StatLinePluginInterface {
    $results = [];
    foreach ($this->getStats() as $id => $stat) {
      $results[$id] = $stat->compute();
    }
    $this->results[] = (object) $results;
    return $this;
  }

}
