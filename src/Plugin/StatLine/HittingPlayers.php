<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for players that hit.
 *
 * @StatLine(
 *   id = "hitting_players",
 *   name = @Translation("Hitting Players"),
 *   table = "hitting_players",
 *   default_sort = "total",
 *   stat_order = {
 *     "name",
 *     "year",
 *     "teams",
 *     "positions",
 *     "rank",
 *     "position_rank",
 *     "games",
 *     "hits",
 *     "at_bats",
 *     "runs",
 *     "home_runs",
 *     "rbis",
 *     "stolen_bases",
 *     "batting_average",
 *     "power_speed",
 *     "runs_total",
 *     "home_runs_total",
 *     "rbis_total",
 *     "stolen_bases_total",
 *     "batting_average_total",
 *     "total",
 *     "total_per_game",
 *     "hits_projected",
 *     "at_bats_projected",
 *     "batting_average",
 *     "rbis_projected",
 *     "runs_projected",
 *     "home_runs_projected",
 *     "stolen_bases_projected",
 *   },
 * )
 */
class HittingPlayers extends StatLinePluginBase {

  /**
   * {@inheritdoc}
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface {
    $filters = [
      'name' => [],
      'year' => [],
      'positions' => [
        'operator' => 'LIKE',
      ],
      'games_min' => [
        'operator' => '>=',
        'column' => 'games',
      ],
      'games_max' => [
        'operator' => '<=',
        'column' => 'games',
      ],
    ];
    if (!empty($modifiers['filters'])) {
      foreach ($filters as $key => $filter) {
        if (!empty($modifiers['filters'][$key])) {
          $filter += [
            'column' => $key,
            'operator' => '=',
          ];
          $modifier = $modifiers['filters'][$key];
          unset($modifiers['filters'][$key]);
          $modifiers['filters'][] = [
            'column' => $filter['column'],
            'value' => $filter['operator'] == 'LIKE' ? '%' . $modifier . '%' : $modifier,
            'operator' => $filter['operator'],
          ];
        }
      }
    }
    return parent::setModifiers($modifiers);
  }

}
