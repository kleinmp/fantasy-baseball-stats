<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for daily hitting.
 *
 * @StatLine(
 *   id = "hitting_daily",
 *   name = @Translation("Hitting Daily"),
 *   table = "hitting_daily",
 *   stat_order = {
 *     "date_year",
 *     "month",
 *     "day_of_month",
 *     "day_of_week'"
 *     "date",
 *     "days",
 *     "hits",
 *     "at_bats",
 *     "runs",
 *     "home_runs",
 *     "rbis",
 *     "stolen_bases",
 *     "batting_average",
 *     "runs_total",
 *     "home_runs_total",
 *     "rbis_total",
 *     "stolen_bases_total",
 *     "batting_average_total",
 *     "total",
 *     "total_per_days",
 *     "total_per_est_games",
 *   },
 * )
 */
class HittingDaily extends StatLinePluginBase {

  /**
   * {@inheritdoc}
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface {
    if (!empty($modifiers['filters'])) {
      if (!empty($modifiers['filters']['year'])) {
        $modifiers['filters'][] = [
          'column' => 'date',
          'value' => $modifiers['filters']['year'] . '-01-01',
          'operator' => '>=',
        ];
        $modifiers['filters'][] = [
          'column' => 'date',
          'value' => $modifiers['filters']['year'] . '-12-31',
          'operator' => '<=',
        ];
        unset($modifiers['filters']['year']);
      }
    }
    return parent::setModifiers($modifiers);
  }

}
