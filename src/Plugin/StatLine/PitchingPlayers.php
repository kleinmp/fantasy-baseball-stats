<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for players that pitch.
 *
 * @StatLine(
 *   id = "pitching_players",
 *   name = @Translation("Pitching Players"),
 *   table = "pitching_players",
 *   default_sort = "total",
 *   stat_order = {
 *     "name",
 *     "year",
 *     "teams",
 *     "positions",
 *     "rank",
 *     "games",
 *     "innings_pitched",
 *     "wins",
 *     "saves",
 *     "strikeouts",
 *     "strikeout_rate",
 *     "earned_runs",
 *     "base_runners",
 *     "era",
 *     "whip",
 *     "wins_total",
 *     "saves_total",
 *     "strikeouts_total",
 *     "era_total",
 *     "whip_total",
 *     "total",
 *     "total_per_game",
 *   },
 * )
 */
class PitchingPlayers extends StatLinePluginBase {

  /**
   * {@inheritdoc}
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface {
    $filters = [
      'name' => [],
      'year' => [],
      'positions' => [
        'operator' => 'LIKE',
      ],
      'games_min' => [
        'operator' => '>=',
        'column' => 'games',
      ],
      'games_max' => [
        'operator' => '<=',
        'column' => 'games',
      ],
    ];
    if (!empty($modifiers['filters'])) {
      foreach ($filters as $key => $filter) {
        if (!empty($modifiers['filters'][$key])) {
          $filter += [
            'column' => $key,
            'operator' => '=',
          ];
          $modifier = $modifiers['filters'][$key];
          unset($modifiers['filters'][$key]);
          $modifiers['filters'][] = [
            'column' => $filter['column'],
            'value' => $filter['operator'] == 'LIKE' ? '%' . $modifier . '%' : $modifier,
            'operator' => $filter['operator'],
          ];
        }
      }
    }
    return parent::setModifiers($modifiers);
  }

}
