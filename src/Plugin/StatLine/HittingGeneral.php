<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for general hitting.
 *
 * @StatLine(
 *   id = "hitting_general",
 *   name = @Translation("Hitting General"),
 *   stat_order = {
 *     "games",
 *     "hits",
 *     "at_bats",
 *     "runs",
 *     "home_runs",
 *     "rbis",
 *     "stolen_bases",
 *     "batting_average",
 *     "runs_total",
 *     "home_runs_total",
 *     "rbis_total",
 *     "stolen_bases_total",
 *     "batting_average_total",
 *     "total",
 *     "total_per_game",
 *   },
 * )
 */
class HittingGeneral extends StatLinePluginBase {}
