<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for general pitching.
 *
 * @StatLine(
 *   id = "pitching_general",
 *   name = @Translation("Pitching General"),
 *   stat_order = {
 *     "games",
 *     "innings_pitched",
 *     "wins",
 *     "saves",
 *     "strikeouts",
 *     "strikeout_rate",
 *     "earned_runs",
 *     "base_runners",
 *     "era",
 *     "whip",
 *     "wins_total",
 *     "saves_total",
 *     "strikeouts_total",
 *     "era_total",
 *     "whip_total",
 *     "total",
 *     "total_per_game",
 *   },
 * )
 */
class PitchingGeneral extends StatLinePluginBase {}
