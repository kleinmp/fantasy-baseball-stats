<?php

namespace Drupal\fbase\Plugin\StatLine;

/**
 * Provides stat line for daily pitching.
 *
 * @StatLine(
 *   id = "pitching_daily",
 *   name = @Translation("Pitching Daily"),
 *   table = "pitching_daily",
 *   stat_order = {
 *     "date_year",
 *     "month",
 *     "day_of_month",
 *     "day_of_week",
 *     "date",
 *     "days",
 *     "innings_pitched",
 *     "wins",
 *     "saves",
 *     "strikeouts",
 *     "earned_runs",
 *     "base_runners",
 *     "era",
 *     "whip",
 *     "strikeout_rate",
 *     "wins_total",
 *     "saves_total",
 *     "strikeouts_total",
 *     "era_total",
 *     "whip_total",
 *     "total",
 *     "total_per_days",
 *   },
 * )
 */
class PitchingDaily extends StatLinePluginBase {

  /**
   * {@inheritdoc}
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface {
    if (!empty($modifiers['filters'])) {
      if (!empty($modifiers['filters']['year'])) {
        $modifiers['filters'][] = [
          'column' => 'date',
          'value' => $modifiers['filters']['year'] . '-01-01',
          'operator' => '>=',
        ];
        $modifiers['filters'][] = [
          'column' => 'date',
          'value' => $modifiers['filters']['year'] . '-12-31',
          'operator' => '<=',
        ];
        unset($modifiers['filters']['year']);
      }
    }
    return parent::setModifiers($modifiers);
  }

}
