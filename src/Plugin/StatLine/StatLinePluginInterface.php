<?php

namespace Drupal\fbase\Plugin\StatLine;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Database\Connection;
use Drupal\fbase\Plugin\Stat\StatManager;

/**
 * Defines an interface for a stat line.
 */
interface StatLinePluginInterface extends PluginInspectionInterface {

  /**
   * Return the name of the name of the service.
   *
   * @return string
   *   The name of the stat line.
   */
  public function getName() : string;

  /**
   * Get the database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  public function getDatabase() : Connection;

  /**
   * Get the stat manager service.
   *
   * @return \Drupal\fbase\Plugin\Stat\StatManager
   *   The stat manager.
   */
  public function getStatManager() : StatManager;

  /**
   * Get the related stats.
   *
   * @return array
   *   Array of \Drupal\fbase\Plugin\Stat\StatPluginInterface
   */
  public function getStats() : array;

  /**
   * Get the table header data.
   *
   * @return array
   *   The table header.
   */
  public function getHeader() : array;

  /**
   * Get the results from the query execution.
   *
   * @return array
   *   An array of results.
   */
  public function results() : array;

  /**
   * Whether the data is stored in a db table or must be computed.
   *
   * @return bool
   *   Whether or not data is stored in db table.
   */
  public function statsInDb() : bool;

  /**
   * Get the titles of all related stats.
   *
   * @return array
   *   An array of titles.
   */
  public function getStatTitles() : array;

  /**
   * Get the sql expressions from the related stats.
   *
   * @return array
   *   Array of sql expressions.
   */
  public function getStatMetaData() : array;

  /**
   * Execute the query to get all stat data.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function executeQuery() : StatLinePluginInterface;

  /**
   * Add a modifier to filter/sort/group the query results.
   *
   * @param string $type
   *   The type of modifier (filter, sort, group, group_type)
   * @param mixed $modifier
   *   The value of the modifier.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function addModifier(string $type, $modifier) : StatLinePluginInterface;

  /**
   * Set the modifier a value.
   *
   * @param string $type
   *   The type of modifier (filter, sort, group, group_type)
   * @param array $modifiers
   *   The value of the modifier.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function setModifier(string $type, array $modifiers) : StatLinePluginInterface;

  /**
   * Set the value on all of the modifiers.
   *
   * @param array $modifiers
   *   An array containing all of the modifiers.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function setModifiers(array $modifiers) : StatLinePluginInterface;

  /**
   * Set the value on related stats.
   *
   * @param array $stat_values
   *   An array of values keyed by the stat id.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function setStatValues(array $stat_values) : StatLinePluginInterface;

  /**
   * Calculate the computed values on the stats.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface
   *   The current object.
   */
  public function compute() : StatLinePluginInterface;

}
