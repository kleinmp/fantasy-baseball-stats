<?php

namespace Drupal\fbase\Plugin\StatLine;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manage StatLines and Stats.
 */
class StatLineManager extends DefaultPluginManager {

  /**
   * Constructs an StatLineManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/StatLine',
      $namespaces,
      $module_handler,
      'Drupal\fbase\Plugin\StatLine\StatLinePluginInterface',
      'Drupal\fbase\Annotation\StatLine'
    );

    $this->alterInfo('fbase_stat_line_info');
    $this->setCacheBackend($cache_backend, 'fbase.stat_line');
  }

}
