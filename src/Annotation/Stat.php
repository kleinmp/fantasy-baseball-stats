<?php

namespace Drupal\fbase\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a stat annotation object.
 *
 * Plugin Namespace: Drupal\fbase\Plugin\Stat.
 *
 * @see \Drupal\fbase\Plugin\StatManager
 * @see plugin_api
 *
 * @Annotation
 */
class Stat extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the stat.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * Whether a higher or lower number is better (min or max).
   *
   * @var string|null
   */
  protected $best = 'max';

  /**
   * The name of the related sql table column.
   *
   * @var string|null
   */
  protected $column = NULL;

  /**
   * Whether or not this is a computed value.
   *
   * @var bool
   */
  protected $computed = FALSE;

  /**
   * The value used to compute the total value.
   *
   * @var int|float|null
   */
  protected $constant = NULL;

  /**
   * The number of decimals to display on this number.
   *
   * @var int|null
   */
  protected $decimals = 0;

  /**
   * Whether or not to display the stat.
   *
   * @var bool
   */
  protected $display = TRUE;

  /**
   * The sql alias to use.
   *
   * @var string
   */
  protected $expression_alias = '';

  /**
   * The plugin ids of the required stats.
   *
   * @var array
   */
  protected $required_stat_ids = [];

  /**
   * Whether or not this is used to compute the total score.
   *
   * @var bool
   */
  protected $total = FALSE;

}
