<?php

namespace Drupal\fbase\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a stat line annotation object.
 *
 * Plugin Namespace: Drupal\fbase\Plugin\StatLine.
 *
 * @see \Drupal\fbase\Plugin\StatLine\StatLineManager
 * @see plugin_api
 *
 * @Annotation
 */
class StatLine extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the stat line.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The sql database name related to this stat line.
   *
   * @var string
   */
  protected $table;

  /**
   * List of related stat ids in display order.
   *
   * @var array
   */
  protected $stat_order;

  /**
   * The name of the stat to sort by default.
   *
   * @var string
   */
  protected $default_sort;

}
