<?php

namespace Drupal\fbase\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile to import csv data.
 */
class FbaseCommands extends DrushCommands {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new FbaseCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler) {
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Import daily hitting csv.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   *
   * @throws \Exception
   *
   * @command fbase:import-hitting-daily
   * @aliases fihd
   */
  public function importDailyHitting(string $file) :void {
    $this->importDaily($file, 'hitting');
  }

  /**
   * Import daily pitching csv.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   *
   * @throws \Exception
   *
   * @command fbase:import-pitching-daily
   * @aliases fipd
   */
  public function importDailyPitching(string $file) : void {
    $this->importDaily($file, 'pitching');
  }

  /**
   * Import players hitting csv.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   *
   * @throws \Exception
   *
   * @command fbase:import-hitting-players
   * @aliases fihp
   */
  public function importPlayersHitting(string $file) : void {
    $this->importPlayers($file, 'hitting');
  }

  /**
   * Import players pitching csv.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   *
   * @throws \Exception
   *
   * @command fbase:import-pitching-players
   * @aliases fipp
   */
  public function importPlayersPitching(string $file) : void {
    $this->importPlayers($file, 'pitching');
  }

  /**
   * Get the current database conneciton.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  public function getDatabase() {
    return $this->database;
  }

  /**
   * Import daily baseball data.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   * @param string $type
   *   The type of data contained in the file (pitching or hitting).
   *
   * @throws \Exception
   */
  public function importDaily(string $file, string $type) : void {
    $this->moduleHandler->loadInclude('fbase');
    if (!file_exists($file)) {
      throw new \Exception(sprintf('%s does not exit.', $file));
    }

    $handle = fopen($file, "r");
    if (!$handle) {
      throw new \Exception(sprintf('%s cannot be opened.', $file));
    }

    $schema = fbase_schema()[$type . '_daily'];
    $header = fgetcsv($handle, 2000, ",");
    while ($stat_line = fgetcsv($handle, 2000, ",")) {
      $fields = array_combine($header, $stat_line);
      $fields['date'] = date('Y-m-d', strtotime($fields['date']));
      $fields = array_intersect_key($fields, $schema['fields']);
      $this->getDatabase()->merge($type . '_daily')
        ->key(['date' => $fields['date']])
        ->fields($fields)
        ->execute();
    }
    $this->logger()->success(dt('Data imported.'));
  }

  /**
   * Import player baseball data.
   *
   * @param string $file
   *   The path to the csv file containing the data.
   * @param string $type
   *   The type of data contained in the file (pitching or hitting).
   *
   * @throws \Exception
   */
  public function importPlayers(string $file, string $type) : void {
    $this->moduleHandler->loadInclude('fbase');
    if (!file_exists($file)) {
      throw new \Exception(sprintf('%s does not exit.', $file));
    }

    $handle = fopen($file, "r");
    if (!$handle) {
      throw new \Exception(sprintf('%s cannot be opened.', $file));
    }

    $schema = fbase_schema()[$type . '_players'];
    $header = fgetcsv($handle, 2000, ",");
    while ($stat_line = fgetcsv($handle, 2000, ",")) {
      $fields = array_combine($header, $stat_line);
      $fields = array_intersect_key($fields, $schema['fields']);
      $fields['rank'] = !empty($fields['rank']) ? $fields['rank'] : NULL;
      $fields['position_rank'] = !empty($fields['position_rank']) ? $fields['position_rank'] : NULL;
      $this->getDatabase()->merge($type . '_players')
        ->key(['name' => $fields['name'], 'year' => $fields['year']])
        ->fields($fields)
        ->execute();
    }
    $this->logger()->success(dt('Data imported.'));
  }

}
