<?php

namespace Drupal\fbase\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\fbase\Plugin\StatLine\StatLineManager;
use Drupal\fbase\Plugin\StatLine\StatLinePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for totals pages.
 */
class Totals extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler service.
   *
   * @var \Drupal\fbase\Plugin\StatLine\StatLineManager
   */
  protected $statLineManager;

  /**
   * Constructs a Totals controller object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   * @param \Drupal\fbase\Plugin\StatLine\StatLineManager $stat_line_manager
   *   The stat line manager service.
   */
  public function __construct(RequestStack $request_stack, StatLineManager $stat_line_manager) {
    $this->requestStack = $request_stack;
    $this->statLineManager = $stat_line_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('plugin.manager.fbase.stat_line_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'fbase';
  }

  /**
   * Get the request stack.
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  public function getRequest() {
    return $this->requestStack;
  }

  /**
   * Get the stat line manager.
   *
   * @return \Drupal\fbase\Plugin\StatLine\StatLineManager
   *   The stat line manager.
   */
  public function getStatLineManager() : StatLineManager {
    return $this->statLineManager;
  }

  /**
   * Page callback for displaying stat line data.
   *
   * @param string $stat_line_type
   *   A valid stat line plugin id.
   *
   * @return array
   *   The page render array.
   */
  public function statLineDisplay($stat_line_type) : array {
    $stat_line = $this->getStatLineManager()->createInstance($stat_line_type);
    $results = $this->getStats($stat_line);
    return $this->buildTotalsTable($results, $stat_line->getStatMetaData(), $stat_line->getHeader());
  }

  /**
   * Page callback for displaying stat line computed data.
   *
   * @param string $stat_line_type
   *   A valid stat line plugin id.
   *
   * @return array
   *   The page render array.
   */
  public function statLineComputedDisplay($stat_line_type) : array {
    $stat_line = $this->getStatLineManager()->createInstance($stat_line_type);
    $results = $this->getStatValues() ? $this->getStats($stat_line) : [];

    $table = $this->buildTotalsTable($results, $stat_line->getStatMetaData(), $stat_line->getHeader());
    $form = $this->formBuilder()->getForm('Drupal\fbase\Form\ComputeScore', $stat_line_type);

    return [
      'form' => $form,
      'table' => $table,
    ];
  }

  /**
   * Get the stats data from a query on a stat line.
   *
   * @param \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface $stat_line
   *   The stat line on which to query the stats.
   *
   * @return array
   *   The data results from the stats.
   */
  public function getStats(StatLinePluginInterface $stat_line) : array {
    if ($stat_line->statsInDb()) {
      $results = $this->getQueriedStats($stat_line);
    }
    else {
      $results = $this->getComputedStats($stat_line);
    }
    return $results;
  }

  /**
   * Get the stats data from a query on a stat line.
   *
   * @param \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface $stat_line
   *   The stat line on which to query the stats.
   *
   * @return array
   *   The computed results of the query.
   */
  public function getQueriedStats(StatLinePluginInterface $stat_line) : array {
    $modifiers = $this->getModifiers();
    return $stat_line->setModifiers($modifiers)
      ->executeQuery()
      ->results();
  }

  /**
   * Get the stats data from entered data on a stat line.
   *
   * @param \Drupal\fbase\Plugin\StatLine\StatLinePluginInterface $stat_line
   *   The stat line on which to compute the stats.
   *
   * @return array
   *   The computed results.
   */
  public function getComputedStats(StatLinePluginInterface $stat_line) : array {
    return $stat_line->setStatValues($this->getStatValues())
      ->compute()
      ->results();
  }

  /**
   * Build a render array table with stat data.
   *
   * @return array
   *   The rendered totals table.
   */
  public function buildTotalsTable(array $results, array $stat_data, array $header) : array {
    // $build['#cache']['contexts'] = ['url.path', 'url.query_args:start', 'url.query_args:end'];
    $count = 0;
    $best = [];
    foreach ($results as $key => $line) {
      foreach ($line as $row_key => $value) {
        if (isset($stat_data[$row_key]['decimals'])) {
          $results[$key]->{$row_key} = number_format((float) $value, $stat_data[$row_key]['decimals'], '.', '');
        }
        if (isset($stat_data[$row_key]['best'])) {
          if (!isset($best[$row_key])) {
            $best[$row_key] = $value;
          }
          elseif ($stat_data[$row_key]['best'] == 'max') {
            $best[$row_key] = $value > $best[$row_key] ? $value : $best[$row_key];
          }
          elseif ($stat_data[$row_key]['best'] == 'min') {
            $best[$row_key] = $value < $best[$row_key] ? $value : $best[$row_key];
          }
        }
      }
    }

    $build['table'] = [
      '#type' => 'table',
      '#empty' => $this->t('There are no results.'),
      '#header' => array_merge([''], $header),
      '#sticky' => TRUE,
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    foreach ($results as $key => $line) {
      $build['table']['#rows'][$key] = ['data' => []];
      $build['table']['#rows'][$key]['data']['count']['data'] = ++$count;
      foreach ($line as $row_key => $value) {
        if (isset($stat_data[$row_key]['display']) && !$stat_data[$row_key]['display']) {
          continue;
        }
        if (isset($best[$row_key]) && $value == $best[$row_key]) {
          $value = '<strong>' . $value . '</strong>';
        }
        $build['table']['#rows'][$key]['data'][$row_key]['data'] = ['#markup' => $value];
      }
    }

    return $build;
  }

  /**
   * Get the query parameters.
   *
   * @return array
   *   The query modifiers if they exist.
   */
  public function getModifiers() : array {
    $modifiers = [];

    // @todo This does not set the group_type.
    foreach (['filters', 'groups'] as $type) {
      if ($modifier = $this->getRequest()->getCurrentRequest()->query->get($type)) {
        $modifiers[$type] = $modifier;
      }
    }
    return $modifiers;
  }

  /**
   * Get the currently submitted stat values.
   *
   * @return array
   *   An array keyed by stat ids with values.
   */
  public function getStatValues() : array {
    $stats = $this->getRequest()->getCurrentRequest()->query->get('stat_values');
    return !empty($stats) ? $stats : [];
  }

}
