# Fantasy Baseball Stats

A Drupal module that imports and displays fantasy baseball data.

## Getting started

Install as you would install a Drupal module.

## Import data

Use drush to import from a csv file. Each table has a different command for import. See install file for tables and see csvs directory for example csv files.
```
fbase:import-hitting-daily [path-to-csv]
fbase:import-pitching-daily [path-to-csv]
fbase:import-hitting-players [path-to-csv]
fbase:import-pitching-players [path-to-csv]
```
